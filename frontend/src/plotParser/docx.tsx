import JSZip from "jszip";

const domParser = new DOMParser();

const extractDocxLines = async (docx: Uint8Array): Promise<string[]> => {
  const unzipped = await JSZip.loadAsync(docx);
  const document = await unzipped
    .folder(`word`)
    ?.file(`document.xml`)
    ?.async(`string`);

  if (document === undefined) {
    throw new Error(`Invalid .docx file`);
  }

  const lines: string[] = [];
  const root = domParser.parseFromString(document, `application/xml`);
  const body = root.firstChild?.firstChild;

  if (!body) {
    throw new Error(`Invalid .docx file`);
  }

  for (const child of body.childNodes) {
    if (child.nodeName !== `w:p`) {
      continue;
    }

    let paragraph = ``;

    for (const grandchild of child.childNodes) {
      if (grandchild.nodeName !== `w:r`) {
        continue;
      }

      for (const grandgrandchild of grandchild.childNodes) {
        if (grandgrandchild.nodeName === `w:t`) {
          paragraph += grandgrandchild.textContent;
        }
      }
    }

    lines.push(paragraph.replace(/\n/g, ` `));
  }

  return lines;
};

export default extractDocxLines;
