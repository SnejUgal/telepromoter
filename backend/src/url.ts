import { networkInterfaces } from "os";

export const PORT = 2000;

const interfaces = networkInterfaces();
let ipV4;
let ipV6;

for (const addresses of Object.values(interfaces)) {
  if (!addresses) {
    continue;
  }

  for (const { address, internal, family } of addresses) {
    if (internal) {
      continue;
    }

    if (family === `IPv4`) {
      ipV4 = address;
    } else {
      ipV6 = `[${address}]`;
    }
  }
}

export const ip = ipV4 ?? ipV6 ?? `[::]`;
export const url = `${ip}:${PORT}`;
