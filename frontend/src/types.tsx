export interface Line {
  role: string;
  text: string;
}

export type Plot = Line[];
