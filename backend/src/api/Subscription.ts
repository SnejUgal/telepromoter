import { CURRENT_LINE_UPDATE, events, Line, PLOT_UPDATE } from "./state";

export const watchPlot = {
  resolve: (parent: Line) => parent,
  subscribe: () => events.asyncIterator(PLOT_UPDATE),
};

export const watchCurrentLine = {
  resolve: (line: number | null) => line,
  subscribe: () => events.asyncIterator(CURRENT_LINE_UPDATE),
};
