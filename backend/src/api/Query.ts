import { url } from "../url";
import { currentLine, Plot, plot } from "./state";

export const getPlot = (): Plot => plot;
export const getCurrentLine = (): number | null => currentLine;
export const getServerUrl = (): string => url;
