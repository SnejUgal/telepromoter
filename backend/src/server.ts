import express = require("express");
import { createServer } from "http";
import * as path from "path";
import api from "./api";
import { PORT } from "./url";

const app = express();

const frontendPath = path.join(
  __dirname,
  `../node_modules/@telepromoter/frontend/build`
);
app.use(express.static(frontendPath));
api.applyMiddleware({ app, path: `/api` });

const server = createServer(app);
api.installSubscriptionHandlers(server);

const start = () =>
  new Promise<void>((resolve) => {
    server.listen(PORT, `::`, () => resolve());
  });

export default start;
