const { default: startServer } = require(`@telepromoter/backend/build/server`);
const { url } = require(`@telepromoter/backend/build/url`);
const { app, BrowserWindow } = require(`electron`);
const path = require(`path`);

let window;
const appReady = new Promise((resolve) => app.on(`ready`, resolve));
app.on(`window-all-closed`, () => app.quit());

Promise.all([startServer(), appReady]).then(() => {
  window = new BrowserWindow({
    icon: path.join(__dirname, `./icon.png`),
    autoHideMenuBar: true,
  });
  window.loadURL(`http://${url}`);
  window.on(`closed`, () => {
    window = null;
  });
});
