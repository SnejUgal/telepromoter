import { ApolloServer, gql } from "apollo-server-express";
import { readFileSync } from "fs";
import * as path from "path";

import * as Mutation from "./api/Mutation";
import * as Query from "./api/Query";
import * as Subscription from "./api/Subscription";

const typeDefinitionsPath = path.join(__dirname, `../api.graphql`);
const typeDefinitions = gql(readFileSync(typeDefinitionsPath, `utf8`));

const resolvers = {
  Mutation,
  Query,
  Subscription,
};

export default new ApolloServer({
  typeDefs: typeDefinitions,
  resolvers,
  subscriptions: { path: `/api` },
});
