import { PubSub } from "apollo-server-express";
import { randomBytes } from "crypto";

export interface Line {
  role: string;
  text: string;
}

export type Plot = Line[];

export let adminToken: string | null = null;
export let plot: Plot = [];
export let currentLine: number | null = null;

export const events = new PubSub();
export const PLOT_UPDATE = `plotUpdate`;
export const CURRENT_LINE_UPDATE = `currentLineUpdate`;

export const refreshAdminToken = (): string => {
  adminToken = randomBytes(16).toString(`hex`);
  return adminToken;
};

export const setPlot = (newPlot: Plot) => {
  plot = newPlot;
  events.publish(PLOT_UPDATE, plot);

  const oldCurrentLine = currentLine;
  currentLine =
    plot.length > 0 ? Math.min(currentLine ?? 0, plot.length - 1) : null;
  if (currentLine !== oldCurrentLine) {
    events.publish(CURRENT_LINE_UPDATE, currentLine);
  }
};

export const setCurrentLine = (line: number) => {
  if (!plot) {
    throw new Error(`No text set`);
  }
  if (line < 0 || line >= plot.length) {
    throw new Error(`New current line is out of bounds`);
  }

  currentLine = line;
  events.publish(CURRENT_LINE_UPDATE, currentLine);
};
