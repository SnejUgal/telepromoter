import { useMutation, useQuery, useSubscription } from "@apollo/client";
import { loader } from "graphql.macro";
import { useCallback, useState } from "react";
import client from "../client";

const GET_CURRENT_LINE = loader("./getCurrentLine.graphql");
const WATCH_CURRENT_LINE = loader("./watchCurrentLine.graphql");
const SET_CURRENT_LINE = loader("./setCurrentLine.graphql");

interface GetCurrentLine {
  currentLine: number | null;
}

interface Props {
  token: string | null;
}

const useCurrentLineIndex = ({ token }: Props) => {
  const [currentLineIndex, setCurrentLineIndex] = useState<number | null>(null);

  useQuery<GetCurrentLine>(GET_CURRENT_LINE, {
    client,
    onCompleted: ({ currentLine }) => setCurrentLineIndex(currentLine),
  });

  useSubscription<GetCurrentLine>(WATCH_CURRENT_LINE, {
    client,
    onSubscriptionData({ subscriptionData: { data } }) {
      if (!token && data) {
        setCurrentLineIndex(data.currentLine);
      }
    },
  });

  const [mutateCurrentLine] = useMutation(SET_CURRENT_LINE, { client });

  const updateCurrentLineIndex = useCallback(
    (newIndex: number) => {
      setCurrentLineIndex(newIndex);
      mutateCurrentLine({ variables: { token, line: newIndex } });
    },
    [token, mutateCurrentLine]
  );

  return { currentLineIndex, setCurrentLineIndex: updateCurrentLineIndex };
};

export default useCurrentLineIndex;
