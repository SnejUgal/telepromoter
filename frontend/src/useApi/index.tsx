import { useMemo } from "react";
import { Line, Plot } from "../types";
import useCurrentLineIndex from "./useCurrentLineIndex";
import usePlot from "./usePlot";
import useServerUrl from "./useServerUrl";
import useStatus from "./useStatus";

interface UserApi {
  serverUrl: string | null;
  plot: Plot;
  currentLineIndex: number | null;
  currentLine: Line | null;
  roles: Set<string>;
}

interface AdminApi extends UserApi {
  setPlot(newPlot: Plot): void;
  setCurrentLineIndex(newCurrentLineIndex: number): void;
}

type Api =
  | { status: `unknown` }
  | ({ status: `user` } & UserApi)
  | ({ status: `admin` } & AdminApi);

const useApi = (): Api => {
  const status = useStatus();
  const token = `token` in status ? status.token : null;

  const serverUrl = useServerUrl();
  const { plot, setPlot } = usePlot({ token });
  const { currentLineIndex, setCurrentLineIndex } = useCurrentLineIndex({
    token,
  });

  const currentLine = currentLineIndex == null ? null : plot[currentLineIndex];
  const roles = useMemo(() => {
    return new Set(plot.map(({ role: screen }) => screen));
  }, [plot]);

  if (status.status === `unknown`) {
    return { status: `unknown` };
  }

  const base = {
    serverUrl,
    plot,
    currentLineIndex,
    currentLine,
    roles,
  };

  if (status.status === `user`) {
    return { ...base, status: `user` };
  }

  return {
    ...base,
    status: `admin`,
    setPlot,
    setCurrentLineIndex,
  };
};

export default useApi;
