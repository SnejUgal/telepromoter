const extractTxtLines = (txt: Uint8Array): string[] => {
  const string = [...txt]
    .map((codePoint) => String.fromCodePoint(codePoint))
    .join();

  return string.split(`\n`);
};

export default extractTxtLines;
