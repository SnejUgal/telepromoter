import { ApolloClient, HttpLink, InMemoryCache, split } from "@apollo/client";
import { WebSocketLink } from "@apollo/client/link/ws";
import { getMainDefinition } from "@apollo/client/utilities";

const httpLink = new HttpLink({ uri: `/api` });

const subscriptionUrlParsed = new URL(window.location.origin);
subscriptionUrlParsed.protocol = `ws:`;
subscriptionUrlParsed.pathname = `/api`;
const subscriptionUrl = subscriptionUrlParsed.toString();
const wsLink = new WebSocketLink({
  uri: subscriptionUrl,
  options: { reconnect: true },
});

const link = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === `OperationDefinition` &&
      definition.operation === `subscription`
    );
  },
  wsLink,
  httpLink
);

const client = new ApolloClient({ link, cache: new InMemoryCache() });

export default client;
