import "./style.scss";

import { useState } from "react";
import classNames from "classnames";
import { Line } from "../types";

interface SelectRoleProps {
  roles: Set<string>;
  onSelect(screen: string): void;
}

const SelectRole = ({ roles, onSelect }: SelectRoleProps) => {
  const [selection, setSelection] = useState<string | null>(null);

  return (
    <div className="roleSelection">
      <h1 className="roleSelection_title">Выберите роль</h1>
      <ul className="roleSelection_select">
        {[...roles].map((role) => {
          const className = classNames(`roleSelection_option`, {
            "-active": role === selection,
          });

          return (
            <li key={role} value={role}>
              <label
                className={className}
                onClick={() => setSelection(role)}
                tabIndex={0}
              >
                <input type="radio" name="role" />
                {role}
              </label>
            </li>
          );
        })}
      </ul>
      <button
        className="roleSelection_apply"
        disabled={selection === null}
        onClick={() => onSelect(selection!)}
      >
        Выбрать
      </button>
    </div>
  );
};

interface RoleProps {
  roles: Set<string>;
  currentLine: Line | null;
}

const Role = ({ roles, currentLine }: RoleProps) => {
  const [role, setRole] = useState<string | null>(null);

  if (roles.size === 0) {
    return null;
  }

  if (role === null || !roles.has(role)) {
    return (
      <SelectRole
        roles={roles}
        onSelect={(role) => {
          setRole(role);
          document.documentElement.requestFullscreen();
        }}
      />
    );
  }

  if (currentLine?.role !== role) {
    return null;
  }

  return <p className="text">{currentLine.text}</p>;
};

export default Role;
