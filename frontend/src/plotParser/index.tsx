import { Plot } from "../types";
import extractDocxLines from "./docx";
import extractTxtLines from "./txt";

const processLines = (lines: string[]): Plot => {
  const canonicalRoleSpellings: Set<string> = new Set();
  const plot = [];

  for (const rawLine of lines) {
    const line = rawLine.trim().replace(/\s{2,}/g, ` `);

    const closingParentesisIndex = line.indexOf(`)`);
    if (!line.startsWith(`(`) || closingParentesisIndex === -1) {
      continue;
    }

    let role = line.slice(1, closingParentesisIndex).trim();

    let canonicalRoleSpelling;
    for (const name of canonicalRoleSpellings) {
      if (name.toLowerCase() === role.toLowerCase()) {
        canonicalRoleSpelling = name;
      }
    }

    if (canonicalRoleSpelling) {
      role = canonicalRoleSpelling;
    } else {
      canonicalRoleSpellings.add(role);
    }

    const text = line.slice(closingParentesisIndex + 1).trim();

    if (text === ``) {
      continue;
    }

    plot.push({ role, text });
  }

  return plot;
};

const parsePlot = async (
  fileName: string,
  contents: Uint8Array
): Promise<Plot> => {
  let lines: string[];

  if (fileName.endsWith(`.docx`)) {
    lines = await extractDocxLines(contents);
  } else if (fileName.endsWith(`.txt`)) {
    lines = extractTxtLines(contents);
  } else {
    throw new Error(`Unsupported file format`);
  }

  return processLines(lines);
};

export default parsePlot;
