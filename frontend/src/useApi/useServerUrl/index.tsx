import { useQuery } from "@apollo/client";
import { loader } from "graphql.macro";
import client from "../client";

const GET_SERVER_URL = loader("./getServerUrl.graphql");
interface GetServerUrl {
  serverUrl: string;
}

const useServerUrl = () => {
  const { data } = useQuery<GetServerUrl>(GET_SERVER_URL, { client });

  return data?.serverUrl ?? null;
};

export default useServerUrl;
