import { AuthenticationError, UserInputError } from "apollo-server-express";
import * as state from "./state";
import { Plot } from "./state";

export const becomeAdmin = () => {
  if (state.adminToken) {
    return null;
  }

  return state.refreshAdminToken();
};

interface SetPlotArguments {
  token: string;
  plot: Plot;
}

export const setPlot = (_: unknown, args: SetPlotArguments) => {
  if (args.token !== state.adminToken) {
    throw new AuthenticationError(`Invalid token`);
  }

  state.setPlot(args.plot);
  return true;
};

interface SetCurrentLineArguments {
  token: string;
  line: number;
}

export const setCurrentLine = (_: unknown, args: SetCurrentLineArguments) => {
  if (args.token !== state.adminToken) {
    throw new AuthenticationError(`Invalid token`);
  }

  try {
    state.setCurrentLine(args.line);
  } catch (error) {
    throw new UserInputError(error.message);
  }

  return true;
};
