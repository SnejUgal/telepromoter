import startServer from "./server";
import { url } from "./url";

startServer().then(() => console.log(`Listening on ${url}...`));
