import { useMutation, useQuery, useSubscription } from "@apollo/client";
import { loader } from "graphql.macro";
import { useCallback, useState } from "react";
import { Plot } from "../../types";
import client from "../client";

const GET_PLOT = loader("./getPlot.graphql");
const WATCH_PLOT = loader("./watchPlot.graphql");
const SET_PLOT = loader("./setPlot.graphql");

interface GetPlot {
  plot: Plot;
}

interface Props {
  token: string | null;
}

const usePlot = ({ token }: Props) => {
  const [plot, setPlot] = useState<Plot>([]);

  useQuery<GetPlot>(GET_PLOT, {
    client,
    onCompleted: ({ plot }) => setPlot(plot),
  });

  useSubscription<GetPlot>(WATCH_PLOT, {
    client,
    onSubscriptionData({ subscriptionData: { data } }) {
      if (!token && data) {
        setPlot(data.plot);
      }
    },
  });

  const [mutatePlot] = useMutation(SET_PLOT, { client });

  const updatePlot = useCallback(
    (plot: Plot) => {
      setPlot(plot);
      mutatePlot({ variables: { token, plot } });
    },
    [mutatePlot, token]
  );

  return { plot, setPlot: updatePlot };
};

export default usePlot;
