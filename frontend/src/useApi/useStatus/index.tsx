import { useMutation } from "@apollo/client";
import { loader } from "graphql.macro";
import { useEffect, useState } from "react";
import client from "../client";

type Status =
  | { status: `unknown` }
  | { status: `user` }
  | { status: `admin`; token: string };

const BECOME_ADMIN = loader("./becomeAdmin.graphql");
interface BecomeAdmin {
  token?: string;
}

const useStatus = () => {
  const [status, setStatus] = useState<Status>({ status: `unknown` });

  const [becomeAdmin] = useMutation<BecomeAdmin>(BECOME_ADMIN, {
    client,
    onCompleted({ token }) {
      setStatus(token ? { status: `admin`, token } : { status: `user` });
    },
  });

  useEffect(() => {
    becomeAdmin();
  }, [becomeAdmin]);

  return status;
};

export default useStatus;
