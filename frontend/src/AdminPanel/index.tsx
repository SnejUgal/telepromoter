import "./style.scss";

import { ChangeEvent, useCallback, useEffect, useRef } from "react";
import { Plot } from "../types";
import classNames from "classnames";
import parsePlot from "../plotParser";

const readFile = (file: File): Promise<Uint8Array> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.addEventListener(`load`, () =>
      resolve(new Uint8Array(reader.result as ArrayBuffer))
    );
    reader.addEventListener(`error`, () => reject(reader.error));

    reader.readAsArrayBuffer(file);
  });

const GO_BACK_KEYS = [
  `ArrowUp`,
  `ArrowLeft`,
  `KeyW`,
  `KeyA`,
  `KeyH`,
  `KeyK`,
  `Numpad8`,
  `Numpad4`,
];
const GO_NEXT_KEYS = [
  `ArrowDown`,
  `ArrowRight`,
  `KeyS`,
  `KeyD`,
  `KeyL`,
  `KeyJ`,
  `Numpad2`,
  `Numpad6`,
];
const GO_TO_START_KEYS = [`Home`, `Numpad7`];
const GO_TO_END_KEYS = [`End`, `Numpad1`];

interface Props {
  plot: Plot;
  currentLine: number | null;
  onCurrentLineChange(newCurrentLine: number): void;
  onPlotChange(newPlot: Plot): void;
  serverUrl: string | null;
}

const AdminPanel = ({
  plot,
  currentLine,
  onCurrentLineChange,
  onPlotChange,
  serverUrl,
}: Props) => {
  const linesRef = useRef<HTMLUListElement>(null);
  const changeLine = useCallback(
    (line) => {
      onCurrentLineChange(line);

      const nextLineElement = linesRef.current!.children[line];
      const { top, height } = nextLineElement.getBoundingClientRect();
      const absoluteTopEdge = window.scrollY + top;
      const relativeTopEdge = (window.innerHeight - height) / 2;

      window.scrollTo({
        top: Math.max(0, absoluteTopEdge - relativeTopEdge),
        behavior: `smooth`,
      });
    },
    [onCurrentLineChange]
  );

  useEffect(() => {
    if (plot.length === 0) {
      return;
    }

    const handleKeyDown = (event: KeyboardEvent) => {
      if (currentLine === null) {
        return;
      }

      let nextLine;
      if (GO_BACK_KEYS.includes(event.code) && currentLine > 0) {
        nextLine = currentLine - 1;
      } else if (
        GO_NEXT_KEYS.includes(event.code) &&
        currentLine < plot.length - 1
      ) {
        nextLine = currentLine + 1;
      } else if (GO_TO_START_KEYS.includes(event.code)) {
        nextLine = 0;
      } else if (GO_TO_END_KEYS.includes(event.code)) {
        nextLine = plot.length - 1;
      }

      if (nextLine !== undefined) {
        event.preventDefault();
        changeLine(nextLine);
      }
    };

    document.body.addEventListener(`keydown`, handleKeyDown);

    return () => document.body.removeEventListener(`keydown`, handleKeyDown);
  }, [plot, currentLine, changeLine]);

  const handleImport = async (event: ChangeEvent<HTMLInputElement>) => {
    if (event.currentTarget.files?.length !== 1) {
      return;
    }

    const [file] = event.currentTarget.files;
    event.currentTarget.value = ``;

    const unparsedPlot = await readFile(file);
    const plot = await parsePlot(file.name, unparsedPlot);

    onPlotChange(plot);

    if (currentLine === null) {
      onCurrentLineChange(0);
    }
  };

  if (plot.length === 0) {
    return (
      <div className="importScreen">
        <h1 className="importScreen_title">
          Откройте сценарий в&nbsp;формате .docx или&nbsp;.txt
        </h1>
        <label className="importScreen_button" tabIndex={0}>
          <input type="file" onChange={handleImport} accept=".txt,.docx" />
          Выбрать файл
        </label>
      </div>
    );
  }

  return (
    <div className="adminPanel">
      <header className="header">
        {serverUrl && (
          <div className="serverUrl">
            <span className="serverUrl_label">URL-адрес для подключения</span>
            <span className="serverUrl_url">{serverUrl}</span>
          </div>
        )}
        <div className="header_buttons">
          <label className="adminPanel_replacePlot" tabIndex={0}>
            <input type="file" onChange={handleImport} accept=".txt,.docx" />
            Заменить сценарий
          </label>
        </div>
      </header>
      <ul className="adminPanel_lines" ref={linesRef}>
        {plot.map((line, index) => {
          const className = classNames(`line`, {
            "-active": index === currentLine,
          });

          return (
            <li
              className={className}
              key={index}
              onClick={() => changeLine(index)}
            >
              <h2 className="line_role">{line.role}</h2>
              <p className="line_text">{line.text}</p>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default AdminPanel;
