import "./style.scss";

import AdminPanel from "../AdminPanel";
import Role from "../Role";
import useApi from "../useApi";

const App = () => {
  const api = useApi();

  if (api.status === `unknown`) {
    return null;
  }

  if (api.status === `user`) {
    return <Role currentLine={api.currentLine} roles={api.roles} />;
  }

  return (
    <AdminPanel
      plot={api.plot}
      currentLine={api.currentLineIndex}
      onPlotChange={api.setPlot}
      onCurrentLineChange={api.setCurrentLineIndex}
      serverUrl={api.serverUrl}
    />
  );
};

export default App;
